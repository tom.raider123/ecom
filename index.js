const express = require('express');
// const mongoose = require('mongoose');
const app = express();
require('dotenv').config()
app.use(express.json());
const bodyParser = require('body-parser');
// const cookieParser = require('cookie-parser');
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
// app.use(cookieParser())
const connectDB = require('./db/connect');
const login = require('./routes/loginRoute');
const product = require('./routes/productRoute');
const user = require('./routes/userRoute');

// routes
// app.get('/api/v1/ecom')
// app.post('/api/v1/ecom') 
// app.post('/api/v1/ecom')



app.use('/api/v1/login',login)
app.use('/api/v1/product',product)
app.use('/api/v1/user',user)
const port = 3001;  

const start  = async () =>{
  try{
    await connectDB(process.env.MONGO_URI) 
    app.listen(port,console.log(`server running on ${port}`));
  }catch(error){
    console.log(error);
  }
}

start(); 









// mongoose.connect('mongodb://localhost:27017/demo', {
//   useNewUrlParser: true, 
//   useUnifiedTopology: true
// }, err => {
//   if(err) throw err;
//   console.log("Connected to mongodb")
// })




app.post("/add_user", async (request, response) => {
  response.send(request.body);
  const user = new userModel(request.body);

  try {
    await user.save();
    response.send(user);
  } catch (error) {
    response.status(500).send(error);
  }
});

 