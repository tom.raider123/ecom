const express = require('express');
const router = express.Router();
const {
    UserLogin,
    UserSignup
} = require('../controllers/loginController');

router.route('/').get(UserLogin);
router.route('/').post(UserSignup);
module.exports = router
