const product = require('../models/user');

const getUser = (req,res)=>{
    res.json('getUser');
}

const getAllUser = (req,res)=>{
    res.json('getAllUser');
}

const deleteUser = (req,res) => {
    res.json('deleteUser'); 
}

const updateUser = (req,res) => {
    res.json('update user'); 
}

const saveUser = (req,res) => {
    res.json('save user'); 
}

module.exports = {
    getUser,
    getAllUser,
    deleteUser,
    updateUser,
    saveUser
}