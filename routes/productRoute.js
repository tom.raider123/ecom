const express = require('express');
const router =  express.Router();
const {
    getAllproduct,
    getProduct,
    saveProduct,
    updateProduct,
    deleteProduct
} = require('../controllers/productController');
router.route('/').get(getAllproduct);
router.route('/').post(saveProduct);
router.route('/:id').get(getProduct);
router.route('/:id').patch(updateProduct);
router.route('/:id').delete(deleteProduct);

module.exports = router
