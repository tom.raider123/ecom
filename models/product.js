const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema({
    name:{
        type:String,
        required: true,
    },
    mrp:{
        type:Number,
        required:true,
    },
    image:{
        type:String,
        required:true,
    },
    description:{
        type:String,
        required:true,
        maxlength :50,
    }
})

const Product = mongoose.model("Product",ProductSchema);

module.exports = Product;
