const mongoose = require("mongoose");


const UserSchema = new mongoose.Schema({
  firstname: {
    type: String,
    required: true,
    marlength: 100,
  },
  lastname: {
    type: String,
    required: true,
    marlength: 100,
  },
  age: {
    type: Number,
    default: 0,
  },
  email: {
    type: String,
    required: true,
    trim: true,
    unique : true
  },
  address: {
    type: String,
  },
  user_type:{
    type:Number,
    required: true,
  },
  password:{
    type:String,
    required: true,
    minlength: 8,
    maxlength: 25,
  },
  token:{
    type: String,
  }
});

const User = mongoose.model("User", UserSchema);

module.exports = User;