const express = require('express');
const router = express.Router();
const {
    getUser,
    getAllUser,
    deleteUser,
    updateUser,
    saveUser
} = require('../controllers/userController');
router.route('/').get(getAllUser).post(saveUser)
router.route('/:id').get(getUser).patch(updateUser).delete(deleteUser)


module.exports = router 
