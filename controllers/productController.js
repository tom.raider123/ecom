const Product = require('../models/product');

const getAllproduct =  async(req,res) => {
     try{
        const allproduct = await Product.find();

        if(!allproduct){
            res.status(404).json({msg: "Not found!"})
        }else{
            res.status(200).json({allproduct})
        }
     }catch(err){
        res.status(500).json({msg:err})
     }
}

const getProduct = async (req,res) => {
   try{
     const product = await Product.findOne({_id : req.params.id});
     if(!product){
        res.status(404).json({msg: "Not found !"})
     }else{
        res.status(404).json({product})
     }
   }catch(err){
     res.status(500).json({msg:err});
   }
}

const saveProduct = async(req,res) => {  
    try{
        const product =  await Product.create(req.body)
        if(!product){
            res.status(500).json({msg:"Some thing went wrong!"})
        }else{
            res.status(201).json({product})
        }
    }catch(err){
        res.status(500).json({msg: err})
    }
}

const updateProduct = async(req,res) => {    
  try{

    const _id = req.params.id;
    const product = await Product.findByIdAndUpdate(_id , req.body, {new : true});
    if(!product){
     res.status(404).json({msg: "product not found!"})
    }else{
        res.status(201).json({msg : product})
    }
  }catch(err){
    res.status(500).json({msg:err});

  }
}

const deleteProduct = async(req,res) => {
    try{
    const product = await Product.deleteOne({_id : req.params.id});
    if(!product){
        res.status(404).json({msg: "Something went wrong !"})
    }else{
        res.status(201).json({product})
    }
    }catch(err){
      res.status(500).json({msg:err});
    }       
    res.status('delete product') 
}

module.exports = {
    getAllproduct,
    getProduct,
    saveProduct,
    updateProduct,
    deleteProduct
}